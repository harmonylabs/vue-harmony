import commonjs from '@rollup/plugin-commonjs';
import vue from 'rollup-plugin-vue';
import buble from '@rollup/plugin-buble';

export default {
    input: 'src/index.js',
    output: {
        name: 'VueHarmony',
        exports: 'named',
        globals: {
            vue: 'Vue',
        },
    },
    plugins: [
        commonjs(),
        vue({
            css: true,
            compileTemplate: true
        }),
        buble(),
    ],
    external: [
        'vue',
        'axios',
        'v-click-outside',
        'lodash/includes',
        'lodash/findKey'
    ],
};
