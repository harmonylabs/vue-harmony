import VHAlert from './components/VHAlert.vue'

export default {
    install(Vue, args = {}) {
        if (this.installed) return;

        this.installed = true
        this.params = args

        Vue.component('vh-alert', VHAlert)

        const alert = params => {
            window.EventBus.$emit('open', params)
        }

        Vue.prototype.$alert = alert
        Vue['$alert'] = alert
    }
}
