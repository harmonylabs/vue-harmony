import VHInput from './components/VHInput.vue';
import VHTable from './components/VHTable.vue';
import VHUpload from './components/VHUpload.vue';
import VHSelect from './components/VHSelect.vue';
import VHToggle from './components/VHToggle.vue';
import VHTabs from './components/VHTabs.vue';
import VHTab from './components/VHTab.vue';
import VHModal from './components/VHModal.vue';
import VHMenu from './components/VHMenu.vue';
import VHAlert from './alert';
import VHConfirm from './confirm';

export function install (Vue) {
    if (install.installed) return;

    install.installed = true;

    Vue.component('vh-input', VHInput)
    Vue.component('vh-table', VHTable)
    Vue.component('vh-upload', VHUpload)
    Vue.component('vh-select', VHSelect)
    Vue.component('vh-toggle', VHToggle)
    Vue.component('vh-tabs', VHTabs)
    Vue.component('vh-tab', VHTab)
    Vue.component('vh-modal', VHModal)
    Vue.component('vh-menu', VHMenu)

    Vue.use(VHAlert)
    Vue.use(VHConfirm)
}

// create module definition for Vue.use()
const plugin = {
    install
};

// auto install
let GlobalVue = null
if (typeof window !== 'undefined') {
    GlobalVue = window.Vue
} else if (typeof global !== 'undefined') {
    GlobalVue = global.Vue
}

if (GlobalVue) {
    GlobalVue.use(plugin)
}

export default plugin;
