# Vue-Harmony

VueHarmony is a set of simple Vue components created to cater the Harmony Platform eCommerce Laravel Package.

## Insallation
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
